#Step 1: Create an empty file named ” HelloWorld ” in the terminal
#We can create an empty file using the following commands:

$ touch HelloWorld.sh
#Step 2: Open file
#Here we will open our created file, using the following commands: 

$ nano HelloWorld.sh
#The editor is open, write the following code in a text editor:

#!/bin/sh
# This is bash program to display Hello World
echo " Hello World "
#Step 3: Run the script
$ ./Helloworld.sh 

